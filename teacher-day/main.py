from manim import *
from manim_fonts import *

default_font = 'Raleway'
class TeachersDay(Scene):
    def construct(self):
        with RegisterFont('Pacifico') as fonts:
            text = Text("Happy Teachers Day", font=fonts[0], font_size=80)
        self.play(FadeIn(text), run_time=4)
        self.play(text.animate.scale(.4))
        self.play(text.animate.move_to([4.5 , -3.5, 0]))

        with RegisterFont(default_font) as fonts: 
            text = MarkupText("You're the best teacher I've come across!", font=fonts[0])
        self.play(FadeIn(text))
        self.wait(1)
        self.play(FadeOut(text))


        with RegisterFont(default_font) as fonts:
            text = MarkupText("We appreciate your time,\n Your patience,\n Your ability to make dry subject intersting,\n and your <span bgcolor='WHITE' fgcolor='BLACK'>smile</span>", font=fonts[0], font_size=30, justify=True)
        self.play(FadeIn(text))
        self.wait(1)
        self.play(FadeOut(text))


        with RegisterFont(default_font) as fonts:
            text = Text("Thank You For Everything!", font_size=60, font=fonts[0])
        self.play(FadeIn(text))
        self.wait(1)
        self.play(FadeOut(text))


        with RegisterFont(default_font) as fonts:
            text = MarkupText("From <span background='BLUE' fgcolor='WHITE'>Naveen</span>", font=fonts[0])
        self.play(FadeIn(text))
        self.wait(1)

