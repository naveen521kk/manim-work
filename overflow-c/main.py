from __future__ import annotations
from re import M

from manim import *
from manim_fonts import *
from style import *
import functools

DEFAULT_FONT_SIZE = 30
DEFAULT_TEXT_COLOR = TEXT_COLOR
config.background_color = BACKGROUND_COLOR

_font = RegisterFont("Arvo")
arvo_font = _font.__enter__()


def StyledMarkupText(text, **kwargs):
    default = {
        "color": TEXT_COLOR,
        "font": arvo_font[0],
        "font_size": DEFAULT_FONT_SIZE,
    }
    default.update(kwargs)
    return MarkupText(text, **default)


def StyledTex(tex, **kwargs):
    default = {
        "color": TEXT_COLOR,
    }
    default.update(kwargs)
    return Tex(tex, **default)


def StyledMathTex(tex, **kwargs):
    default = {
        "color": TEXT_COLOR,
    }
    default.update(kwargs)
    return MathTex(tex, **default)


class MaxVal(Scene):
    def render_list_texts(self, *texts):
        return StyledMarkupText("".join([f"- {i}\n" for i in texts]))

    def fade_in_and_move(self, obj, next_to_mobj, pos=DOWN, anim=FadeIn):
        self.play(anim(obj))
        self.play(obj.animate.next_to(next_to_mobj, pos, buff=0.5))

    def in_and_out(
        self, *objs: list[Mobject], wait_time=2, in_anim=FadeIn, out_anim=FadeOut
    ):
        for i in objs:
            self.play(in_anim(i))
        self.wait(wait_time)
        for j in reversed(objs):
            self.play(out_anim(j))

    def construct(self):
        with RegisterFont("Mukta") as f1:
            bt = StyledMarkupText(
                "<u>char</u>",
                font_size=DEFAULT_FONT_SIZE + (DEFAULT_FONT_SIZE * 0.8),
                font=f1[0],
            )
        self.play(FadeIn(bt))
        self.play(bt.animate.move_to(UP * 2))
        char_size = StyledMarkupText(
            "Size in bytes: 1",
            font_size=DEFAULT_FONT_SIZE + (DEFAULT_FONT_SIZE * 0.3),
        )
        self.fade_in_and_move(char_size, bt)
        find_range_t = StyledMarkupText(
            "<b><u>How to find it's range?</u></b>",
            font_size=DEFAULT_FONT_SIZE + (DEFAULT_FONT_SIZE * 0.8),
        )
        self.play(Write(find_range_t))
        self.play(FadeOut(bt), FadeOut(char_size), )
        self.play(Unwrite(find_range_t))

        bytes_conv = Group(
            StyledTex("1 byte = 8 bits").scale(1.4),
            StyledTex(f"8 bits can store $2^8 = {2**8}$ 0s and 1s").scale(1.4),
        )
        bytes_conv.arrange(direction=DOWN)
        self.in_and_out(*bytes_conv)

        _t_font_size = DEFAULT_FONT_SIZE + (DEFAULT_FONT_SIZE * 0.3)
        _chars = VGroup(
            StyledMarkupText(
                f"If <span color='{CYAN}'><tt>unsigned char</tt></span> "
                "then it can store from",
                font_size=_t_font_size,
            ),
            StyledTex(f"0 to $2^8$", font_size=_t_font_size + 20, color=RED),
        ).arrange(direction=DOWN)
        self.play(Write(_chars))
        _t = StyledTex(f"0 to {2**8}", color=RED).next_to(_chars[0], DOWN)
        self.play(ReplacementTransform(_chars[1], _t))
        _chars[1] = _t
        self.play(FadeOut(_chars))

        _chars = VGroup(
            StyledMarkupText(
                f"If <span color='{CYAN}'><tt>char</tt></span> is <span color='{CYAN}'><tt>signed</tt></span> "
                "then it can store from",
                font_size=_t_font_size,
            ),
            StyledTex(
                f"$-2^{{8 - 1}}$ to $2^{{8 - 1}}$",
                font_size=_t_font_size + 20,
                color=RED,
            ),
        ).arrange(direction=DOWN)
        self.play(Write(_chars))
        _t = StyledTex("$-2^7$ to $2^7$", color=RED).next_to(_chars[0], DOWN)
        self.play(ReplacementTransform(_chars[1], _t))
        _chars[1] = _t
        _t = StyledTex(f"-{2**7} to {2**7}", color=RED).next_to(_chars[0], DOWN)
        self.play(ReplacementTransform(_chars[1], _t))
        _chars[1] = _t
        self.play(FadeOut(_chars))
