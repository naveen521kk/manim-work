from manim import *
from manim_fonts import *

config.background_color = WHITE
DEFAULT_TEXT_COLOR = BLACK
HANDWRITING_FONT = "Indie Flower"


class LogVideo(Scene):
    def write_and_unwrite(self, text) -> None:
        self.play(Write(text))
        self.wait(0.5)
        self.play(Unwrite(text))

    def fade_in_out(self, text, **kwargs) -> None:
        self.play(FadeIn(text), **kwargs)
        self.wait(0.7)
        self.play(FadeOut(text), **kwargs)

    def create_text(self, text, **kwargs) -> Text:
        if "font_size" not in kwargs:
            kwargs["font_size"] = 55
        with RegisterFont(HANDWRITING_FONT) as handwriting_fonts:
            return Text(
                text,
                font=handwriting_fonts[0],
                color=DEFAULT_TEXT_COLOR,
                **kwargs,
            )

    def create_tex(self, text, **kwargs) -> Text:
        if "font_size" not in kwargs:
            kwargs["font_size"] = 55
        return Tex(
            text,
            color=DEFAULT_TEXT_COLOR,
            **kwargs,
        )

    def create_title(self, text, underline=True, **kwargs) -> MarkupText:
        if "font_size" not in kwargs:
            kwargs["font_size"] = 55
        if underline:
            text = f"<u>{text}</u>"
        with RegisterFont(HANDWRITING_FONT) as handwriting_fonts:
            return MarkupText(
                text,
                font=handwriting_fonts[0],
                color=DEFAULT_TEXT_COLOR,
                **kwargs,
            )

    def construct(self):
        self.write_and_unwrite(self.create_text("What is Logarithms?"))
        self.fade_in_out(
            self.create_tex(
                r"When we are given the base 2, the exponent 3, \\"
                r"we can calculate\\ $2^3=8$.",
            )
        )
        self.fade_in_out(
            self.create_tex(
                r"Inversely, if we are given base 2 and "
                r"its power 8,\\ and we need to find the exponent"
                r"\\ $2^?=8$."
            )
        )
        self.fade_in_out(self.create_tex(r"This exponent is called \emph{logarithm}."))
        self.fade_in_out(
            self.create_tex(
                r'We call the exponent 3, as the \\"logarithm of 8 with base 2"\\'
                r"and write it as\\"
            )
        )
        self.write_and_unwrite(self.create_tex(r"$3 = log_2^8$"))

        self.write_and_unwrite(
            self.create_title(
                "Definition",
                font_size=70,
            )
        )

        self.fade_in_out(
            self.create_tex(
                r"The logarithm of a number m to the base 'a' is the\\"
                r"exponent indicating the power to which the base 'a' \\must be raised to obtain number 'm'.\\"
                r"$log_a^m = x \implies a^x = m$\\"
                r"$a >0, a \neq 1, m > 0$"
            )
        )

class Test(LogVideo):
    def construct(self):
        self.fade_in_out(
            self.create_tex(
                r"The logarithm of a number m to the base 'a' is the\\"
                r"exponent indicating the power to which the base 'a' \\must be raised to obtain number 'm'.\\"
                r"$log_a^m = x \implies a^x = m$\\"
                r"$a >0, a \neq 1, m > 0$"
            )
        )