from manim import *
from pathlib import Path
from manim_fonts import RegisterFont

# solarized color theme
COLOR_THEME = {
    "background": "#fdf6e3",
    "text": "#002b36",
    "yellow": "#b58900",
    "blue": "#268bd2",
    "cyan": "#2aa198",
    "brblack": "#002b36",
    "black": "#073642",
    "brgreen": "#586e75",
    "orange": "#cb4b16",
}

config.background_color = COLOR_THEME["background"]
Tex.set_default(color=COLOR_THEME["text"])
MathTex.set_default(color=COLOR_THEME["text"])
Text.set_default(color=COLOR_THEME["text"])
MarkupText.set_default(color=COLOR_THEME["text"])


class TitleText(Tex):
    def __init__(self, text: str, color=COLOR_THEME["orange"], **kwargs):
        with RegisterFont("Montserrat") as font:
            super().__init__(
                text,
                # font="Montserrat",
                # font_size=30,
                color=color,
                **kwargs,
            )
        self.to_edge(UP)
        underline_width = config["frame_width"] - 2
        underline = Line(LEFT, RIGHT, color=color)
        underline.next_to(self, DOWN, buff=MED_SMALL_BUFF)
        underline.width = underline_width
        self.add(underline)
        self.underline = underline


class ArrowForMobject(Arrow):
    def __init__(
        self, mobject, towards=RIGHT, buff=1, color=COLOR_THEME["text"], **kwargs
    ):
        start = mobject.get_critical_point(towards)
        end = start + (towards) * buff
        super().__init__(start, end, color=color, **kwargs)


class CustomText(Text):
    def __init__(self, text: str, color=COLOR_THEME["text"], **kwargs):
        with RegisterFont("Roboto") as font:
            super().__init__(text, font="Roboto", color=color, **kwargs)


class CustomTex(Tex):
    def __init__(
        self, *tex_strings, font_size=40, color=COLOR_THEME["black"], **kwargs
    ):
        super().__init__(*tex_strings, font_size=font_size, color=color, **kwargs)


class BitWiseColors(Scene):
    def construct(self):
        heading = TitleText("Convert hex colors to RGB array — Bitwise Operators")
        self.play(FadeIn(heading))

        MAIN_COLOR = "#301260"
        RED_COMPONENT = MAIN_COLOR[1:3]
        GREEN_COMPONENT = MAIN_COLOR[3:5]
        BLUE_COMPONENT = MAIN_COLOR[5:7]

        color_text = Tex(
            "\\" + MAIN_COLOR, color=COLOR_THEME["blue"], font_size=50
        ).move_to(  # , disable_ligatures=True
            LEFT
        )
        color_rec = SurroundingRectangle(
            color_text, color=MAIN_COLOR, fill_opacity=0.3, buff=0.4
        )
        self.play(FadeIn(color_rec), Write(color_text))
        self.wait(0.7)

        color_rec_text_grp = Group(color_rec, color_text)
        self.play(color_rec_text_grp.animate.next_to(heading, DOWN))

        red_grp = VDict({MAIN_COLOR[1:3]: color_text[0][1:3]})
        green_grp = VDict({MAIN_COLOR[3:5]: color_text[0][3:5]})
        blue_grp = VDict({MAIN_COLOR[5:7]: color_text[0][5:7]})

        _tmp_dict = {
            "red": red_grp,
            "green": green_grp,
            "blue": blue_grp,
        }
        for name, grp in _tmp_dict.items():
            rec = SurroundingRectangle(grp, color=name, fill_opacity=0.4)
            _text = Tex(name.title() + " component", font_size=40).next_to(
                rec, DOWN * 3
            )
            self.play(FadeIn(rec), Write(_text))
            self.wait(2)
            self.play(FadeOut(rec), FadeOut(_text))

        # red_color_grp = Group(
        #     red_grp.copy().set_color(RED),
        #     Tex("Red component", font_size=40).next_to(red_grp, DOWN),
        # ).next_to(color_rec_text_grp, DOWN)
        # self.play(FadeIn(red_color_grp))
        # self.play(red_color_grp.animate.to_edge(LEFT))

        # arrow_right = Arrow(
        #     red_color_grp.get_right() + (RIGHT / 2),
        #     red_color_grp.get_right() + RIGHT * 2,
        #     color=COLOR_THEME["text"],
        # )
        # arrow_right = ArrowForMobject(red_color_grp, towards=RIGHT, buff=1.5)
        # self.play(FadeIn(arrow_right))
        # hex_val = Tex(f"0x{RED_COMPONENT}", font_size=60).next_to(arrow_right, RIGHT)
        # self.play(Write(hex_val))

        # arrow_right1 = ArrowForMobject(hex_val, towards=RIGHT, buff=1.5)
        # self.play(FadeIn(arrow_right1))
        # hex_val_actual = Tex(int(f"0x{RED_COMPONENT}", base=16), font_size=60).next_to(
        #     arrow_right1, RIGHT
        # )
        # self.play(Write(hex_val_actual))

        # arrow_right2 = ArrowForMobject(hex_val_actual, towards=RIGHT, buff=1.5)
        # self.play(FadeIn(arrow_right2))
        # bin_val = Tex(
        #     bin(int(f"0x{RED_COMPONENT}", base=16))[2:], font_size=60
        # ).next_to(arrow_right2, RIGHT)
        # self.play(Write(bin_val))

        t0 = Table(
            [
                [
                    str(f"0x{i}"),
                    str(int(f"0x{i}", base=16)),
                    str(bin(int(f"0x{i}", base=16))[2:]),
                ]
                for i in (RED_COMPONENT, GREEN_COMPONENT, BLUE_COMPONENT)
            ],
            col_labels=[
                CustomTex("Hexa decimal"),
                CustomTex("Value"),
                CustomTex("Binary"),
            ],
            row_labels=[CustomTex("Red"), CustomTex("Green"), CustomTex("Blue")],
            top_left_entry=CustomTex("Components"),
            element_to_mobject=CustomTex,
        ).next_to(color_rec_text_grp, DOWN)
        t0.get_horizontal_lines().set_color(COLOR_THEME["cyan"])
        t0.get_vertical_lines().set_color(COLOR_THEME["cyan"])
        self.play(FadeIn(t0))

        self.wait(2)

        self.play(FadeOut(t0))
        self.play(FadeOut(color_rec_text_grp))

        info_message = Tex(
            f"\\{MAIN_COLOR} in binary form: “%s”"
            % (bin(int(f"0x{MAIN_COLOR.strip('#')}", base=16)))[2:].zfill(3 * 8)
        ).next_to(heading, DOWN * 2)
        self.play(FadeIn(info_message))

        bin_representation = (bin(int(f"0x{MAIN_COLOR.strip('#')}", base=16)))[
            2:
        ].zfill(3 * 8)
        bin_representation_tex = Tex(
            bin_representation[:8],
            bin_representation[8:16],
            bin_representation[16:],
            font_size=60,
        )
        self.play(FadeIn(bin_representation_tex))
        self.play(bin_representation_tex.animate.next_to(info_message, DOWN))

        red_part, green_part, blue_part = bin_representation_tex.split()
        red_part, green_part, blue_part = (
            red_part.copy(),
            green_part.copy(),
            blue_part.copy(),
        )
        self.play(red_part.animate.set_color(RED).move_to(DOWN + LEFT * 3))
        self.play(green_part.animate.set_color(GREEN).next_to(red_part, RIGHT * 3))
        self.play(blue_part.animate.set_color(BLUE).next_to(green_part, RIGHT * 3))
        all_parts = VGroup(red_part, green_part, blue_part)
        self.play(all_parts.animate.next_to(bin_representation_tex, DOWN * 2))
        self.wait(0.5)

        t1 = Table(
            [
                [i.get_tex_string() for i in bin_representation_tex],
                [int(i.get_tex_string(), base=2) for i in bin_representation_tex],
            ],
            row_labels=[
                CustomTex("Hexa decimal"),
                CustomTex("Decimal Value"),
            ],
            col_labels=[CustomTex("Red"), CustomTex("Green"), CustomTex("Blue")],
            top_left_entry=CustomTex("Colors"),
            element_to_mobject=CustomTex,
        ).next_to(all_parts, DOWN)
        t1.get_horizontal_lines().set_color(COLOR_THEME["cyan"])
        t1.get_vertical_lines().set_color(COLOR_THEME["cyan"])
        self.play(FadeIn(t1))

        self.wait(1)
        self.play(
            FadeOut(info_message),
            FadeOut(bin_representation_tex),
            FadeOut(t1),
            FadeOut(all_parts),
        )

        info_tex = Tex(
            r"For finding the \underline{RED} component, \\we can use the following formula:"
        ).next_to(heading, DOWN * 2)
        self.play(Write(info_tex))

        formula_tex = Tex(
            r"$\alpha >> 16$\\", r"where $\alpha$ is the hexa decimal value."
        ).next_to(info_tex, DOWN * 2)
        formula_tex[1].next_to(formula_tex[0], DOWN * 2)
        self.play(Write(formula_tex))
        tex_info = Tex("Here we do a", r" bitwise right shift.").next_to(
            formula_tex, DOWN * 2
        )
        tex_info[1].set_color(COLOR_THEME["yellow"])
        self.play(Write(tex_info))

        self.wait(2)
        self.play(FadeOut(info_tex), FadeOut(formula_tex), FadeOut(tex_info))

        # self.play(FadeIn(red_part), red_part.animate.next_to(heading, DOWN * 2))
        operation_tex_0 = MathTex(r"\alpha ", ">> 16").next_to(heading, DOWN * 3)
        _tmp_operation_tex_0 = (
            MathTex("\\" + MAIN_COLOR).move_to(operation_tex_0[0]).set_color(MAIN_COLOR)
        )
        self.play(Write(operation_tex_0))
        self.play(
            Transform(operation_tex_0[0], _tmp_operation_tex_0),
            operation_tex_0[1].animate.next_to(_tmp_operation_tex_0, RIGHT),
        )
        self.play(
            operation_tex_0.animate.next_to(heading, DOWN * 2),
        )

        operation_tex_1 = Tex(
            f"${bin_representation_tex.get_tex_string()}$",
            " $>> 16$",
            r"\\(in binary form)",
        ).next_to(operation_tex_0, DOWN * 2)
        self.play(Write(operation_tex_1))
        arrow_down = ArrowForMobject(operation_tex_1, towards=DOWN, buff=1.2)
        self.play(FadeIn(arrow_down))
        _tmp_copy = (
            operation_tex_1[0]
            .copy()
            .next_to(
                arrow_down,
                DOWN,
            )
            .set_color(
                color=RED,
            )
        )
        self.play(FadeIn(_tmp_copy))

        operation_tex_2_1 = Tex(
            str(
                bin(int(MAIN_COLOR.strip("#"), base=16) >> 16),
            )[2:],
        ).move_to(_tmp_copy.get_right() + LEFT)
        self.play(FadeOut(_tmp_copy))
        self.play(FadeIn(operation_tex_2_1))

        operation_tex_2 = Tex(
            str(bin(int(MAIN_COLOR.strip("#"), base=16) >> 16),)[
                2:
            ].zfill(3 * 8),
        ).next_to(arrow_down, DOWN)
        self.play(Transform(operation_tex_2_1, operation_tex_2))

        arrow_down_new = ArrowForMobject(operation_tex_2, towards=DOWN, buff=1.2)
        self.play(FadeIn(arrow_down_new))
        operation_tex_3 = Tex(
            str(
                int(MAIN_COLOR.strip("#"), base=16) >> 16,
            ),
        ).next_to(arrow_down_new, DOWN)
        final_tex = Tex("Red component of the color", color=RED).next_to(
            operation_tex_3, DOWN
        )
        self.play(Write(operation_tex_3))
        self.play(Write(final_tex))
        self.wait(1)

        self.play(
            FadeOut(operation_tex_0),
            FadeOut(operation_tex_1),
            FadeOut(operation_tex_2),
            FadeOut(operation_tex_3),
            FadeOut(final_tex),
            FadeOut(arrow_down),
            FadeOut(arrow_down_new),
            FadeOut(operation_tex_2_1),
        )

        # Green Component
        info_tex = Tex(
            r"For finding the \underline{GREEN} component, \\we can use the following formula:"
        ).next_to(heading, DOWN * 2)
        self.play(Write(info_tex))

        formula_tex = Tex(
            r"$(\alpha >> 8)$ \& $0x00ff$\\",
            r"where $\alpha$ is the hexa decimal value.",
        ).next_to(info_tex, DOWN * 2)
        formula_tex[1].next_to(formula_tex[0], DOWN * 2)
        self.play(Write(formula_tex))
        tex_info = Tex(
            "Here we do a ",
            r" bitwise right shift\\",
            "and a",
            " bitwise AND operation.",
        ).next_to(formula_tex, DOWN * 2)
        tex_info[1].set_color(COLOR_THEME["yellow"])
        tex_info[3].set_color(COLOR_THEME["yellow"])
        self.play(Write(tex_info))

        self.wait(2)
        self.play(FadeOut(info_tex), FadeOut(formula_tex), FadeOut(tex_info))

        # self.play(FadeIn(red_part), red_part.animate.next_to(heading, DOWN * 2))
        operation_tex_0 = Tex("(", r"$\alpha$", r"$>> 8)$ \& $0x00ff$").next_to(
            heading, DOWN * 3
        )
        _tmp_operation_tex_0 = (
            MathTex("\\" + MAIN_COLOR).move_to(operation_tex_0[1]).set_color(MAIN_COLOR)
        )
        self.play(Write(operation_tex_0))
        self.play(
            operation_tex_0[0].animate.next_to(_tmp_operation_tex_0, LEFT),
            Transform(operation_tex_0[1], _tmp_operation_tex_0),
            operation_tex_0[2].animate.next_to(_tmp_operation_tex_0, RIGHT),
        )
        self.play(
            operation_tex_0.animate.next_to(heading, DOWN * 2),
        )

        operation_tex_1 = Tex(
            "(",
            f"${bin_representation_tex.get_tex_string()}$ ",
            rf"$>> 8)$ \& ${bin(0x00ff)[2:].zfill(2 * 8)}$",
            r"\\(in binary form)",
        ).next_to(operation_tex_0, DOWN * 2)
        self.play(Write(operation_tex_1))

        arrow_down = ArrowForMobject(operation_tex_1, towards=DOWN, buff=1)
        self.play(FadeIn(arrow_down))

        # operation_tex_2_1 = Tex(
        #     str(
        #         bin(int(MAIN_COLOR.strip("#"), base=16) >> 16),
        #     )[2:],
        #     color=RED,
        # ).move_to(_tmp_copy.get_right() + LEFT)
        # self.play(FadeOut(_tmp_copy))
        # self.play(FadeIn(operation_tex_2_1))

        operation_tex_2 = Tex(
            f'(${str(bin(int(MAIN_COLOR.strip("#"), base=16) >> 8))[2:].zfill(2 * 8)}$)',
            rf" \& ${bin(0x00ff)[2:].zfill(2 * 8)}$",
        ).next_to(arrow_down, DOWN)
        self.play(Write(operation_tex_2))

        arrow_down_new = ArrowForMobject(operation_tex_2, towards=DOWN, buff=1)
        self.play(FadeIn(arrow_down_new))
        operation_tex_3 = Tex(
            bin((int(MAIN_COLOR.strip("#"), base=16) >> 8) & 0x00FF,)[
                2:
            ].zfill(3 * 8),
            " = ",
            str(
                (int(MAIN_COLOR.strip("#"), base=16) >> 8) & 0x00FF,
            ),
        ).next_to(arrow_down_new, DOWN)
        final_tex = Tex("Green component of the color", color=GREEN).next_to(
            operation_tex_3, DOWN
        )
        self.play(Write(operation_tex_3))
        self.play(Write(final_tex))
        self.wait(1)

        self.play(
            FadeOut(operation_tex_0),
            FadeOut(operation_tex_1),
            FadeOut(operation_tex_2),
            FadeOut(operation_tex_3),
            FadeOut(final_tex),
            FadeOut(arrow_down),
            FadeOut(arrow_down_new),
        )

        # Blue Component
        info_tex = Tex(
            r"For finding the \underline{BLUE} component, \\we can use the following formula:"
        ).next_to(heading, DOWN * 2)
        self.play(Write(info_tex))
        #####################################################################################
        formula_tex = Tex(
            r"$\alpha$ \& $0x0000ff$\\",
            r"where $\alpha$ is the hexa decimal value.",
        ).next_to(info_tex, DOWN * 2)
        formula_tex[1].next_to(formula_tex[0], DOWN * 2)
        self.play(Write(formula_tex))
        tex_info = Tex(
            "Here we simply do a",
            " bitwise AND operation.",
        ).next_to(formula_tex, DOWN * 2)
        tex_info[1].set_color(COLOR_THEME["yellow"])
        self.play(Write(tex_info))

        self.wait(2)
        self.play(FadeOut(info_tex), FadeOut(formula_tex), FadeOut(tex_info))

        # self.play(FadeIn(red_part), red_part.animate.next_to(heading, DOWN * 2))
        operation_tex_0 = Tex(r"$\alpha$", r" \& $0x0000ff$").next_to(heading, DOWN * 3)
        _tmp_operation_tex_0 = (
            MathTex("\\" + MAIN_COLOR).move_to(operation_tex_0[0]).set_color(MAIN_COLOR)
        )
        self.play(Write(operation_tex_0))
        self.play(
            Transform(operation_tex_0[0], _tmp_operation_tex_0),
            operation_tex_0[1].animate.next_to(_tmp_operation_tex_0, RIGHT),
        )
        self.play(
            operation_tex_0.animate.next_to(heading, DOWN * 2),
        )

        operation_tex_1 = Tex(
            f"${bin_representation_tex.get_tex_string()}$ ",
            rf"\& ${bin(0x0000ff)[2:].zfill(3 * 8)}$",
            r"\\(in binary form)",
        ).next_to(operation_tex_0, DOWN * 2)
        self.play(Write(operation_tex_1))

        arrow_down = ArrowForMobject(operation_tex_1, towards=DOWN, buff=1.2)
        self.play(FadeIn(arrow_down))

        # operation_tex_2_1 = Tex(
        #     str(
        #         bin(int(MAIN_COLOR.strip("#"), base=16) >> 16),
        #     )[2:],
        #     color=RED,
        # ).move_to(_tmp_copy.get_right() + LEFT)
        # self.play(FadeOut(_tmp_copy))
        # self.play(FadeIn(operation_tex_2_1))

        operation_tex_2 = Tex(
            bin((int(MAIN_COLOR.strip("#"), base=16)) & 0x0000FF,)[
                2:
            ].zfill(3 * 8),
        ).next_to(arrow_down, DOWN)
        self.play(Write(operation_tex_2))

        arrow_down_new = ArrowForMobject(operation_tex_2, towards=DOWN, buff=1.2)
        self.play(FadeIn(arrow_down_new))
        operation_tex_3 = Tex(
            str(
                (int(MAIN_COLOR.strip("#"), base=16)) & 0x0000FF,
            ),
        ).next_to(arrow_down_new, DOWN)
        final_tex = Tex("Blue component of the color", color=BLUE).next_to(
            operation_tex_3, DOWN
        )
        self.play(Write(operation_tex_3))
        self.play(Write(final_tex))
        self.wait(1)

        self.play(
            FadeOut(operation_tex_0),
            FadeOut(operation_tex_1),
            FadeOut(operation_tex_2),
            FadeOut(operation_tex_3),
            FadeOut(final_tex),
            FadeOut(arrow_down),
            FadeOut(arrow_down_new),
        )

        # Display Code
        code_title = Tex("Code in Python").next_to(heading, DOWN)
        self.play(FadeIn(code_title))

        code = Code(
            "hex_color_py.py", font="Source Code Pro", style="solarized-light"
        ).next_to(code_title, DOWN)
        self.play(FadeIn(code))
        self.wait(2)
        self.play(FadeOut(code_title), FadeOut(code))

        code_title = Tex("Code in C").next_to(heading, DOWN)
        self.play(FadeIn(code_title))
        code = (
            Code(
                "hex_color_c.c",
                font="Source Code Pro",
                style="solarized-light",
            )
            .scale(0.7)
            .next_to(code_title, DOWN)
        )
        self.play(FadeIn(code))
        self.wait(2)
        self.play(FadeOut(code_title), FadeOut(code))

        self.wait(1)
        self.play(FadeOut(heading))
        self.wait(1)
