#include <stdio.h>
#include <stdlib.h>

void convert_hex_to_rgb(char *str, int out_array[3]) {
  // strip trailing `#`
  while (*str == '#')
    str++;
  int hex = (int)strtol(str, NULL, 16);
  out_array[0] = hex >> 16;
  out_array[1] = (hex >> 8) & 0x00FF;
  out_array[2] = hex & 0x0000FF;
}

int main() {
  char *inp = "#301260";
  int op[3];
  convert_hex_to_rgb(inp, op);
  printf("Red: %d\nGreen: %d\nBlue: %d\n", op[0], op[1], op[2]);
}