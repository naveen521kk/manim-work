def convert_hex_to_rgba(hex: str, alpha: float = 1) -> tuple:
    hex = int(hex.strip("#"), base=16)
    return (
        hex >> 16,
        (hex >> 8) & 0x00FF,
        hex & 0x0000FF,
        alpha,
    )


print(convert_hex_to_rgba("#301260"))
