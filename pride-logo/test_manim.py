from manim import *
from pathlib import Path
class BannerTest(Scene):
    def construct(self):
        self.count = 0
        pictures = list(Path('media/images').glob('*.png'))
        self.camera.background_image = pictures[0]
        self.camera.init_background()
        # def test(mob):
        #     try:
        #         self.camera.background_image = pictures[self.count]
        #     except IndexError:
        #         self.count = 0
        #     self.camera.init_background()
        #     self.count += 1
        #     #self.wait(.4)
        #     #self.clear()
        c = ManimBanner(dark_theme=False).scale(2)
        self.add(c)
        self.wait(0.5)
        #c.shapes.add_updater(test)
        self.play(Unwrite(c))
        self.wait(1)
        banner = ManimBanner(dark_theme=False).scale(2)
        #banner.shapes.add_updater(test)
        self.play(banner.create())
        self.wait(1)
