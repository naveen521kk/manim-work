#version 330

uniform vec2 u_resolution;
uniform float u_time;

vec3 rainbow_colors[7];

vec3 colorB = vec3(238/255,130/255,238/255);
vec3 colorA = vec3(1.0,0.0,0.0);


float plot (vec2 st, float pct){
  return  smoothstep( pct-0.01, pct, st.y) -
          smoothstep( pct, pct+0.01, st.y);
}

void set_rainbow_colors(){
    rainbow_colors[0]=vec3(1.0,0.0,0.0);
	rainbow_colors[1]=vec3(1.0,0.6470588235294118,0.0);
	rainbow_colors[2]=vec3(1.0,1.0,0.0);
	rainbow_colors[3]=vec3(0.0,0.5019607843137255,0.0);
	rainbow_colors[4]=vec3(0.0,0.0,1.0);
	rainbow_colors[5]=vec3(0.29411764705882354,0.0,0.5098039215686274);
	rainbow_colors[6]=vec3(0.9333333333333333,0.5098039215686274,0.9333333333333333);
}
vec4 get_color(float comp,float maxx){
    float pct = abs(sin(u_time));
    if (comp<(maxx/7.)){
        vec3 color = mix(rainbow_colors[0], rainbow_colors[1], pct);
        return vec4(color,1.0);
    } else if (comp<(maxx/7.)*2.){
        vec3 color = mix(rainbow_colors[1], rainbow_colors[2], pct);
        return vec4(color,1.0);
    } else if (comp<(maxx/7.)*3.){
        vec3 color = mix(rainbow_colors[2], rainbow_colors[3], pct);
    	return vec4(color,1.0);
    } else if (comp<(maxx/7.)*4.){
        vec3 color = mix(rainbow_colors[3], rainbow_colors[4], pct);
    	return vec4(color,1.0);
    } else if (comp<(maxx/7.)*5.){
        vec3 color = mix(rainbow_colors[4], rainbow_colors[5], pct);
    	return vec4(color,1.0);
    } else if (comp<(maxx/7.)*6.){
        vec3 color = mix(rainbow_colors[5], rainbow_colors[6], pct);
    	return vec4(color,1.0);
    } else if (comp<(maxx/7.)*7.){
        vec3 color = mix(rainbow_colors[6], rainbow_colors[0], pct);
    	return vec4(color,1.0);
    }
    return vec4(1);
}

void main() {
    set_rainbow_colors();
	float pct = sin(u_time);
    //gl_FragColor = get_color(gl_FragCoord.x,u_resolution.x);
    if (pct>0.){
        gl_FragColor = get_color(gl_FragCoord.y,u_resolution.y);
    } else if (pct<0.) {
        gl_FragColor = get_color(gl_FragCoord.x,u_resolution.x);
    } else {
        gl_FragColor = vec4(1);
    }
}