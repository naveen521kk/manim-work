#version 330

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D colors;
out vec4 fragColor;



// vec4 get_color(float comp,float maxx,vec3[7]rainbow_colors){
//     float pct=abs(sin(u_time));
//     if(comp<(maxx/7.)*1){
//         vec3 color=rainbow_colors[0];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*2.){
//         vec3 color=rainbow_colors[1];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*3.){
//         vec3 color=rainbow_colors[2];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*4.){
//         vec3 color=rainbow_colors[3];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*5.){
//         vec3 color=rainbow_colors[4];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*6.){
//         vec3 color=rainbow_colors[5];
//         return vec4(color,1.);
//     }else if(comp<(maxx/7.)*7.){
//         vec3 color=rainbow_colors[6];
//         return vec4(color,1.);
//     }else if(comp==(maxx/7.)*7.){
//         return vec4(1.);
//     }
//     // if(comp<maxx+(maxx/7.)*1){
//     //     vec3 color=rainbow_colors[6];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*2.){
//     //     vec3 color=rainbow_colors[5];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*3.){
//     //     vec3 color=rainbow_colors[4];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*4.){
//     //     vec3 color=rainbow_colors[3];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*5.){
//     //     vec3 color=rainbow_colors[2];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*6.){
//     //     vec3 color=rainbow_colors[1];
//     //     return vec4(color,1.);
//     // }else if(comp<maxx+(maxx/7.)*7.){
//     //     vec3 color=rainbow_colors[0];
//     //     return vec4(color,1.);
//     // }
    
//     return vec4(1.,1.,1.,0.);
// }

vec4 get_color1(float comp,float maxx,vec3[6] rainbow_colors,float alpha){
    float pct = abs(sin(u_time));
    if (comp<(maxx/6.)){
        vec3 color = mix(rainbow_colors[0], rainbow_colors[1], pct);
        return vec4(color,alpha);
    } else if (comp<(maxx/6.)*2.){
        vec3 color = mix(rainbow_colors[1], rainbow_colors[2], pct);
        return vec4(color,alpha);
    } else if (comp<(maxx/6.)*3.){
        vec3 color = mix(rainbow_colors[2], rainbow_colors[3], pct);
    	return vec4(color,alpha);
    } else if (comp<(maxx/6.)*4.){
        vec3 color = mix(rainbow_colors[3], rainbow_colors[4], pct);
    	return vec4(color,alpha);
    } else if (comp<(maxx/6.)*5.){
        vec3 color = mix(rainbow_colors[4], rainbow_colors[5], pct);
    	return vec4(color,alpha);
    } else if (comp<(maxx/6.)*6.){
        vec3 color = mix(rainbow_colors[5], rainbow_colors[0], pct);
    	return vec4(color,alpha);}
    // } else if (comp<(maxx/7.)*7.){
    //     vec3 color = mix(rainbow_colors[6], rainbow_colors[0], pct);
    // 	return vec4(color,alpha);
    // }
    return vec4(1);
}

void main(){
    float pct=sin(u_time);
    vec3 rainbow_colors[6]=vec3[6](
        texelFetch(colors,ivec2(0,0),0).rgb,
        texelFetch(colors,ivec2(1,0),0).rgb,
        texelFetch(colors,ivec2(2,0),0).rgb,
        texelFetch(colors,ivec2(3,0),0).rgb,
        texelFetch(colors,ivec2(4,0),0).rgb,
        texelFetch(colors,ivec2(5,0),0).rgb
        //texelFetch(colors,ivec2(6,0),0).rgb
    );
    //vec3 color_1 = texelFetch(colors, ivec2(0, 0), 0).rgb;
    //vec3 color_2 = texelFetch(colors, ivec2(1, 0), 0).rgb;
    //fragColor = vec4(mix(color_1 , color_2, pct),1.);
    
    // if(pct>0){
    // fragColor=get_color(gl_FragCoord.x/gl_FragCoord.y,u_resolution.x/u_resolution.y,rainbow_colors);}
    // else{
        //     fragColor=get_color(gl_FragCoord.y/gl_FragCoord.x,u_resolution.x/u_resolution.y,rainbow_colors);
    // }
    // if (gl_FragCoord.x > 160){
    //     fragColor = vec4(rainbow_colors[0],1);
    // } else {
    //     fragColor = vec4(.2,.7,.8,1);
    // }
    // if (gl_FragCoord.x<(u_resolution.x/7.)){
    //     vec3 color = mix(rainbow_colors[0], rainbow_colors[1], pct);
    //     fragColor = vec4(.2,.7,.8,1);
    // } else {
    //     fragColor = vec4(rainbow_colors[0],1);
    // }
    fragColor = get_color1(gl_FragCoord.y, u_resolution.y, rainbow_colors,abs(pct));
    //fragColor = vec4(.2,.2,.4,1);
}
