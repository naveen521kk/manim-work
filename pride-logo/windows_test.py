import moderngl_window as mglw
from array import array
import moderngl
from modernglshaders import RAINBOW_COLOR_LIST, get_color_array
class Test(mglw.WindowConfig):
    gl_version = (3, 3)
    window_size = (1920, 1080)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        ctx =self.ctx
        # Do initialization here
        self.prog = self.ctx.program(
            fragment_shader=open("newtest.glsl").read(),
            vertex_shader="""
        #version 330
        in vec2 position;

        void main() {
            gl_Position = vec4(position, 0.0, 1.0);
        }
            """,
        )
        self.prog["colors"] = 0  # read from channel 0
        points = [
            -1,
            1,
            -1,
            -1,
            1,
            1,
            1,
            -1,
        ]
        vbo = ctx.buffer(array("f", points))
        self.vao = ctx.vertex_array(
            self.prog,
            [
                (vbo, "2f", "position"),
            ],
        )
        self.texture = self.ctx.texture(self.wnd.size, 4)

    def render(self, time, frametime):
        c=0
        # This method is called every frame
        ctx = self.ctx
        colors = ctx.texture((7, 1), 3)
        # write some RGB byte values into the textyre
        colors.write(get_color_array(RAINBOW_COLOR_LIST).astype("u1").tobytes())
        colors.use(location=0)
        #self.prog["u_time"] = time
        self.vao.render(moderngl.TRIANGLE_STRIP)
        #data = fbo.read()  # read into pil image or something ..
        #img = Image.frombytes("RGB", fbo.size, data, "raw", "RGB", 0, -1)
        #img.save(f"media/images/{str(frame).zfill(4)}.png")
        #colors.release()
        # if c >= 10:
        #     RAINBOW_COLOR_LIST = RAINBOW_COLOR_LIST[1:] + [RAINBOW_COLOR_LIST[0]]
        #     c = 0
        # else:
        #     c += 1
        # print(c)
        #time += np.pi / 10

# Blocking call entering rendering/event loop
mglw.run_window_config(Test)