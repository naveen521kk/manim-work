import moderngl
import time
from array import array
from PIL import Image
from manim import color_to_rgb
import numpy as np

_rainbow_colour = [
    "#E40303",
    "#FF8C00",
    "#FFED00",
    "#008026",
    "#004DFF",
    "#750787",
    #"#ee82ee",
]
RAINBOW_COLOR_LIST = [color_to_rgb(i) * 255 for i in _rainbow_colour]


def get_color_array(colour):
    return np.array(
        colour,
        dtype="u1",
    )


ctx = moderngl.create_standalone_context()

prog = ctx.program(
    fragment_shader=open("newtest.glsl").read(),
    vertex_shader="""
#version 330
in vec2 position;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
}
    """,
)
#prog["colors"] = 0  # read from channel 0
points = [
    -1,
    1,
    -1,
    -1,
    1,
    1,
    1,
    -1,
]
vbo = ctx.buffer(array("f", points))
vao = ctx.vertex_array(
    prog,
    [
        (vbo, "2f", "position"),
    ],
)
texture = ctx.texture((1080, 1080), 4)

fbo = ctx.framebuffer(color_attachments=[texture])
fbo.use()

prog["u_resolution"] = fbo.size
# prog["_Direction"] = 0
# prog["_WarpTiling"] = 1
# prog["_WarpScale"] = 0
# prog["_Tiling"] = 1

time = 0.1
c = 0
buff = 60
RAINBOW_COLOR_LIST = RAINBOW_COLOR_LIST[::-1]
for frame in range(buff * 3):
    #pprint(RAINBOW_COLOR_LIST)
    # Intialise Colors
    # room for 256 rgb colors
    colors = ctx.texture((6, 1), 3)
    # write some RGB byte values into the textyre
    colors.write(get_color_array(RAINBOW_COLOR_LIST).astype("u1").tobytes())
    colors.use(location=0)
    fbo.clear()
    prog["u_time"] = time
    vao.render(moderngl.TRIANGLE_STRIP)
    data = fbo.read()  # read into pil image or something ..
    img = Image.frombytes("RGB", fbo.size, data, "raw", "RGB", 0, -1)
    img.save(f"media/images/{str(frame).zfill(4)}.png")
    colors.release()
    if c >= buff//2:
        RAINBOW_COLOR_LIST = RAINBOW_COLOR_LIST[1:] + [RAINBOW_COLOR_LIST[0]]
        c = 0
        time = 0
    else:
        c += 1
    #time += np.pi / buff
    time += np.pi / buff
