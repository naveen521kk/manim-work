from manim import *
from utils import *
from solarized import *

MarkupText.set_default(font="Segoe UI", weight=LIGHT)

config.background_color = BASE3


class SquareInvolute(MovingCameraScene):
    side_length = 2.5
    extension_length = 2.5

    def fade_in_and_out(self, mobject: Mobject, **kwargs):
        self.play(FadeIn(mobject, **kwargs))
        self.play(FadeOut(mobject, **kwargs))

    def show_instructions(self, text: str, *extra_objs):
        rectangle = Rectangle(
            height=config.pixel_height,
            width=config.pixel_width,
            color=BASE02,
            fill_opacity=1,
        )
        _t = VGroup(rectangle)
        if text:
            _t.add(MarkupText(text, color=WHITE, justify=True))
        if extra_objs:
            _t.add(*extra_objs)
        self.fade_in_and_out(_t, run_time=3)

    def get_square_with_labels(self):
        sq = Square(self.side_length, color=YELLOW)
        return VGroup(
            sq,
            Tex("A", color=RED, font_size=26).move_to(sq.get_corner(UR) + [0.2, 0, 0]),
            Tex("B", color=RED, font_size=26).move_to(sq.get_corner(UL) + [0, 0.2, 0]),
            Tex("C", color=RED, font_size=26).move_to(sq.get_corner(DL) + [-0.2, 0, 0]),
            Tex("D", color=RED, font_size=26).move_to(sq.get_corner(DR) + [0, -0.2, 0]),
        )

    def construct(self):
        # First draw the required square
        self.show_instructions(
            "Draw a square of length 25mm",
        )
        square = self.get_square_with_labels()
        self.play(square.animate.scale(0.8).move_to(DOWN))

        # then draw extensions of sides of the square
        self.show_instructions("Extend lines CD, DA, AB, BC")
        # first in VGroup is square
        sq_extensions = draw_extensions_of_square(
            square[0], self.extension_length, color=BLUE
        )
        self.play(Create(sq_extensions))

        self.show_instructions(
            "Mark a point P at a distance 100mm \non extension of CD"
        )
        # Zoom the camera to line dividing
        self.play(
            self.camera.frame.animate.move_to(sq_extensions["DR"].get_center()).scale(
                0.7
            )
        )
        point_p = VGroup(
            Dot(sq_extensions["DR"].get_right(), color=RED),
            Tex("P", color=RED, font_size=26).next_to(sq_extensions["DR"].get_right()),
        )
        self.play(Create(point_p))
        self.play(
            self.camera.frame.animate.scale(1.4).move_to(ORIGIN)
        )  # normalize camera

        _txt = MarkupText("Divide DP into 4 equal parts and name it x", color=WHITE)
        self.show_instructions(
            "", _txt, MathTex(r"1’, 2’, 3’, 4’").next_to(_txt, DOWN)
        )
        self.play(
            self.camera.frame.animate.move_to(sq_extensions["DR"].get_center()).scale(
                0.7
            )
        )
        # divide the line at Bottom Right into 4 equal parts
        self.play(
            Create(
                divide_line_equally(
                    sq_extensions["DR"],
                    num_parts=4,
                    color=RED,
                )
            )
        )
        # Normalize camera
        self.play(self.camera.frame.animate.scale(1.4).move_to(ORIGIN))

        self.wait(1)
