from manim.mobject.geometry import Dot
import numpy as np
from solarized import *
from manim import (
    DL,
    DR,
    UL,
    UR,
    Circle,
    Line,
    Square,
    VDict,
    VGroup,
    random_color,
)


def convert_degree_to_radians(inp: float) -> float:
    return inp * (np.pi / 180)


def divide_circle_with_lines(circle: Circle, num_parts: int = 8, **kwargs):
    angle = 360 / num_parts
    left, right = circle.get_left(), circle.get_right()
    lines = VDict({"circle": circle})
    lines = lines.add(
        {
            int(i * angle): Line(left, right, **kwargs)
            .move_to(circle.get_center())
            .rotate(
                convert_degree_to_radians(i * angle),
            )
            for i in range(num_parts)
        }
    )
    return lines


def draw_extensions_of_square(square: Square, extension_length, **kwargs):
    return VDict(
        {
            "DR": Line(
                square.get_corner(DR),
                square.get_corner(DR) + [extension_length * 2, 0, 0],
                **kwargs
            ),
            "UR": Line(
                square.get_corner(UR),
                square.get_corner(UR) + [0, extension_length * 1.5, 0],
                **kwargs
            ),
            "UL": Line(
                square.get_corner(UL),
                square.get_corner(UL) + [-extension_length * 1, 0, 0],
                **kwargs
            ),
            "DL": Line(
                square.get_corner(DL),
                square.get_corner(DL) + [0, -extension_length * 0.5, 0],
                **kwargs
            ),
        }
    )


def divide_line_equally(line: Line, num_parts: int = 4, axis=np.array([1, 0, 0]), **kwargs):
    left = line.get_left()
    length_seg = line.get_length() / num_parts

    
    return VGroup(
        *[
            Dot(
                left + (length_seg * axis * (i + 1)),
                **kwargs
            )
            for i in range(0, num_parts)
        ]
    )
